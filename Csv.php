<?php

class Csv
{
	private $path;
	function __construct($path)
	{
		if(!file_exists($path))  touch($path);
		$this->path = $path;
	}

	public function  save_to_file ($array) {
		if(!is_array($array)) return;
		$handler = fopen($this->path,"w");
		fputcsv($handler, array_keys($array[0]));
foreach ($array as $row) {
/*	fputcsv($handler, array_values($row)); */

	fputcsv($handler, $row);
}
fclose($handler);

	}
}